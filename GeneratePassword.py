import random
dict = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "f", "q", "w", "e", "r", "t", "y", "u", "i", "o", "o",
        "k", "k", "g", "g"]

def genPassword(passLength, dict):
    password = " "
    for i in range(passLength):
        d = random.randint(0, len(dict)-1)
        password += dict[d]
    return password


print(genPassword(5, dict))